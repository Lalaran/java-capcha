package fr.upem.capcha.images;

import java.net.URL;
import java.util.List;

public interface Images {
	List<URL> getPhotos();
	List<URL> getRandomPhotosURL(int number);
	URL getRandomPhotoURL();
	boolean isPhotoCorrect(URL url);
}
