package fr.upem.capcha.images.panneaux;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import fr.upem.capcha.images.Images;
import fr.upem.capcha.ui.MainUi;

public class Panneau implements Images {
	private ArrayList<URL> photos;
	private final int nbPhotos = 3;

	public Panneau() {
		super();
		this.photos = new ArrayList<URL>();
		generateListPhotos();
	}

	public void generateListPhotos(){
		final URL url1 = Panneau.class.getResource("panneaubleu-carre.jpeg");
		photos.add(url1);
		final URL url2 = Panneau.class.getResource("panneau 70.jpg");
		photos.add(url2);
		final URL url3 = Panneau.class.getResource("route panneau.jpg");
		photos.add(url3);
	}
	@Override
	public List<URL> getPhotos() {
		// TODO Auto-generated method stub
		return photos;
	}

	@Override
	public List<URL> getRandomPhotosURL(int number) {
		if(number > photos.size()) {
			throw new ArrayIndexOutOfBoundsException("Not enough pictures in list");
			// throw exception
		}
			
		ArrayList<URL> randomPhotos = new ArrayList<URL>();
		
		while(randomPhotos.size() < number) {
			URL url = getRandomPhotoURL();
			if(!randomPhotos.contains(url))
				randomPhotos.add(url);
		}
		
		return randomPhotos;
	}

	@Override
	public URL getRandomPhotoURL() {
		Random r = new Random();
		r.nextInt(1);
		// final URL url = Panneau.class.getResource("panneaubleu-carre.jpeg");
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean isPhotoCorrect(URL url) {
		// TODO Auto-generated method stub
		return false;
	}

}
